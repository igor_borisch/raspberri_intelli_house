#!/usr/bin/env python
import os
import glob
import time
import Adafruit_DHT
from flask import Flask, render_template, request, jsonify
from ds18b20 import ds18b20

app = Flask(__name__)
sensor = Adafruit_DHT.DHT11
pin = '13'
ds_addr = '28-00000453f070'


@app.route("/temp")
def temp():
    ds1 = ds18b20(ds_addr)
    temp2 = ds1.read_temp()
    humidity,temperature = Adafruit_DHT.read_retry(sensor, pin)
    templateData = {
        'temperature-dht11' : temperature,
        'humidity' : humidity,
        'temperature-ds18b20': temp2
    }
    
    return jsonify(measures=templateData)


if __name__ == '__main__':
    app.run(host='192.168.1.130',port=8081)
